import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('features', { path: '/' });
  this.route('feature', { path: '/feature/2' }); // use :feature_id | when model available
  this.route('not-found', { path: '/*path' }); // 404
});

export default Router;
